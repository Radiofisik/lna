package main;

public class Main {
    public static void main(String[] args) {

        //make demo1 tree
        /*
            A — B — C — D
                 \— E — F — G — J
                  \— H — I    \
                                \— K
         */
        CommitTree.Node root=new CommitTree.Node("A");
        CommitTree commitTree=new CommitTree(root);

        CommitTree.Node b=new CommitTree.Node("B");
        root.addChildren(b);

        CommitTree.Node c=new CommitTree.Node("C");
        CommitTree.Node e=new CommitTree.Node("E");
        CommitTree.Node h=new CommitTree.Node("H");
        b.addChildren(c,e,h);

        CommitTree.Node d=new CommitTree.Node("D");
        c.addChildren(d);

        CommitTree.Node f=new CommitTree.Node("F");
        e.addChildren(f);

        CommitTree.Node i=new CommitTree.Node("I");
        h.addChildren(i);

        CommitTree.Node g=new CommitTree.Node("G");
        f.addChildren(g);

        CommitTree.Node j=new CommitTree.Node("J");
        CommitTree.Node k=new CommitTree.Node("K");
        g.addChildren(j,k);

        CommitTree.Node lca =commitTree.findLCA(j,d);
        System.out.println(lca.getName());
    }
}
