package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommitTree {
    public CommitTree(Node rootNode){
        this.rootNode=rootNode;
    }

    private Node rootNode;

    public Node findLCA(Node a, Node b){
        a.clearAncessorsMark(rootNode);
        a.markAncessors();
        return b.firstCommonAncessorsMarked();
    }

    public Node getRootNode() {
        return rootNode;
    }

    public void setRootNode(Node rootNode) {
        this.rootNode = rootNode;
    }

    public static class Node{
        private String name;
        private Set<Node> childNodes=new HashSet<>();
        private Set<Node> parrentNodes=new HashSet<>();
        private boolean isAncestor;

        public void setAncestor(boolean ancestor) {
            isAncestor = ancestor;
        }

        private void addParrent(Node parrent){
            parrentNodes.add(parrent);
        }

        private void removeParrent(Node parrent){
            parrentNodes.remove(parrent);
        }

        public Node(String name){
            this.name=name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void addChildren(Node ... children){
            for(Node child: children) {
                childNodes.add(child);
                child.addParrent(this);
            }
        }

        public void removeChild(Node child){
            childNodes.remove(child);
            child.removeParrent(this);
        }

        public void markAncessors(){
            setAncestor(true);
            for (Node ancessor: parrentNodes ) {
                ancessor.markAncessors();
            }
        }

        public  void clearAncessorsMark(Node root){
            root.setAncestor(false);
            for(Node child: root.childNodes){
                clearAncessorsMark(child);
            }
        }

        public  Node firstCommonAncessorsMarked(){
            for(Node parrent: parrentNodes){
                if(parrent.isAncestor) return parrent;
                return parrent.firstCommonAncessorsMarked();
            }
            return null;
        }
    }
}
